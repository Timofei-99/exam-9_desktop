import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {NavLink} from "react-router-dom";
import {postNewContacts} from "../../store/actions/contactsAction";
import "./AddForm.css";

const AddContacts = (props) => {
    const dispatch = useDispatch();

    const [contacts, setContacts] = useState({
        name: "",
        phone: "",
        email: "",
        photo: "",
    });


    const contactsDataChanged = (event) => {
        const { name, value } = event.target;

        setContacts((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const contactsHandler = (e) => {
        e.preventDefault();
        dispatch(postNewContacts(contacts));

        setContacts({
            name: "",
            phone: "",
            email: "",
            photo: "",
        });
        props.history.push("/");
    };

    return (
        <>
            <h2>Add New Contacts</h2>
            <form className="FormBlock" onSubmit={contactsHandler}>
                <input
                    name="name"
                    placeholder="Name"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.name}
                />
                <input
                    name="phone"
                    type="text"
                    placeholder="Phone"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.phone}
                />
                <input
                    name="email"
                    placeholder="Email"
                    className="Input"
                    type="email"
                    onChange={contactsDataChanged}
                    value={contacts.email}
                />
                <input
                    name="photo"
                    placeholder="Photo"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.photo}
                />
                {contacts.photo !== "" ? (
                    <div className="ImgBlock">
                        <p>Photo Preview</p>
                        <img src={contacts.photo} alt="Contact" />
                    </div>
                ) : (
                    <div>No Photo</div>
                )}
                <button type="submit" className="FormBtn">
                    Save
                </button>
                <NavLink to="/" className="FormBtn">
                    Back to contacts
                </NavLink>
            </form>
        </>
    );
};

export default AddContacts;
