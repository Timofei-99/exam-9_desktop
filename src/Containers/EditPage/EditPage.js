import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { fetchContact, putContact } from "../../store/actions/contactsAction";
import './EditPage.css';

const EditPage = ({match, history}) => {
    const dispatch = useDispatch();

    const [contacts, setContacts] = useState({
        name: "",
        phone: "",
        email: "",
        photo: "",
    });

    const contact = useSelector((state) => state.contacts.contact);

    useEffect(() => {
        dispatch(fetchContact(match.params.id));
    }, [dispatch, match.params.id]);

    useEffect(() => {
        setContacts(contact);
    }, [contact]);

    const contactsDataChanged = (event) => {
        const { name, value } = event.target;

        setContacts((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const editContact = (e) => {
        e.preventDefault();
        dispatch(putContact(match.params.id, contacts));
        history.push("/");
    };

    return (
        <>
            <h2>Edit Contact</h2>
            <form className="FormBlock" onSubmit={editContact}>
                <input
                    name="name"
                    placeholder="Name"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.name || ""}
                />
                <input
                    name="phone"
                    type="number"
                    placeholder="Phone"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.phone || ""}
                />
                <input
                    name="email"
                    placeholder="Email"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.email || ""}
                />
                <input
                    name="photo"
                    placeholder="Photo"
                    className="Input"
                    onChange={contactsDataChanged}
                    value={contacts.photo || ""}
                />
                {contacts.photo !== "" ? (
                    <div className="ImgBlock">
                        <p>Photo Preview</p>
                        <img src={contacts.photo} alt="Contact" />
                    </div>
                ) : (
                    <div>No Photo</div>
                )}
                <button type="submit" className="FormBtn">
                    Save
                </button>
                <NavLink to="/" className="FormBtn">
                    Back to contacts
                </NavLink>
            </form>
        </>
    );
};

export default EditPage;
