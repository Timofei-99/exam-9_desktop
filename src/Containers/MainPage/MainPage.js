import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import Contact from "../../Component/Contact/Contact";
import {fetchContacts} from "../../store/actions/contactsAction";
import "./MainPage.css";

const ContactsList = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const contacts = useSelector((state) => state.contacts.contacts);

    return (
        <div className="ContactsList">
            {contacts.map((contact, index) => {
                return (
                    <Contact
                        key={contact.id + index}
                        name={contact.name}
                        photo={contact.photo}
                        email={contact.email}
                        phone={contact.phone}
                        id={contact.id}
                    />
                );
            })}
        </div>
    );
};

export default ContactsList;
