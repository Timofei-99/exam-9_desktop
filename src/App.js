import {BrowserRouter, Route, Switch} from "react-router-dom";
// import Layout from "./Components/Layout/Layout";
import AddForm from "./Containers/AddForm/AddForm";
// import EditContact from "./Container/EditContact/EditContact";
import MainPage from './Containers/MainPage/MainPage';
import Header from "./Component/UI/Header/Header";
import EditPage from "./Containers/EditPage/EditPage";

const App = () => (
    <BrowserRouter>
        <Header/>
        <div className="container">
            <Switch>
                <Route path="/" exact component={MainPage}/>
                <Route path="/add" component={AddForm}/>
                <Route path="/edit-contacts/:id" component={EditPage}/>
            </Switch>
        </div>
    </BrowserRouter>

);

export default App;
