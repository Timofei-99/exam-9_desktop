import axiosAPI from "../../axiosApi";

export const POST_CONTACTS_REQUEST = "POST_CONTACTS_REQUEST";
export const POST_CONTACTS_SUCCESS = "POST_CONTACTS_SUCCESS";
export const POST_CONTACTS_FAILURE = "POST_CONTACTS_FAILURE";

export const FETCH_CONTACTS_REQUEST = "FETCH_CONTACTS_REQUEST";
export const FETCH_CONTACTS_SUCCESS = "FETCH_CONTACTS_SUCCESS";
export const FETCH_CONTACTS_FAILURE = "FETCH_CONTACTS_FAILURE";

export const FETCH_CONTACT_REQUEST = "FETCH_CONTACT_REQUEST";
export const FETCH_CONTACT_SUCCESS = "FETCH_CONTACT_SUCCESS";
export const FETCH_CONTACT_FAILURE = "FETCH_CONTACT_FAILURE";

export const PUT_CONTACTS_REQUEST = "PUT_CONTACTS_REQUEST";
export const PUT_CONTACTS_SUCCESS = "PUT_CONTACTS_SUCCESS";
export const PUT_CONTACTS_FAILURE = "PUT_CONTACTS_FAILURE";

export const DELETE_CONTACTS_REQUEST = "DELETE_CONTACTS_REQUEST";
export const DELETE_CONTACTS_SUCCESS = "DELETE_CONTACTS_SUCCESS";
export const DELETE_CONTACTS_FAILURE = "DELETE_CONTACTS_FAILURE";

export const postContactsRequest = () => ({ type: POST_CONTACTS_REQUEST });
export const postContactsSuccess = () => ({ type: POST_CONTACTS_SUCCESS });
export const postContactsFailure = (error) => ({
    type: POST_CONTACTS_FAILURE,
    error,
});

export const fetchContactsRequest = () => ({ type: FETCH_CONTACTS_REQUEST });
export const fetchContactsSuccess = (contacts) => ({
    type: FETCH_CONTACTS_SUCCESS,
    contacts,
});
export const fetchContactsFailure = (error) => ({
    type: FETCH_CONTACTS_FAILURE,
    error,
});

export const fetchContactRequest = () => ({ type: FETCH_CONTACT_REQUEST });
export const fetchContactSuccess = (contact) => ({
    type: FETCH_CONTACT_SUCCESS,
    contact,
});
export const fetchContactFailure = (error) => ({
    type: FETCH_CONTACT_FAILURE,
    error,
});

export const putContactsRequest = () => ({ type: PUT_CONTACTS_REQUEST });
export const putContactsSuccess = () => ({ type: PUT_CONTACTS_SUCCESS });
export const putContactsFailure = (error) => ({
    type: PUT_CONTACTS_FAILURE,
    error,
});

export const deleteContactsRequest = () => ({ type: DELETE_CONTACTS_REQUEST });
export const deleteContactsSuccess = () => ({ type: DELETE_CONTACTS_SUCCESS });
export const deleteContactsFailure = (error) => ({
    type: DELETE_CONTACTS_FAILURE,
    error,
});

export const postNewContacts = (contacts) => {
    return async (dispatch) => {
        try {
            dispatch(postContactsRequest());
            await axiosAPI.post("contacts.json", contacts);
            dispatch(postContactsSuccess());
            dispatch(fetchContacts());
        } catch (error) {
            dispatch(postContactsFailure(error));
        }
    };
};

export const fetchContacts = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchContactsRequest());
            const response = await axiosAPI.get("contacts.json");
            const contacts = Object.keys(response.data).map((id) => ({
                ...response.data[id],
                id,
            }));
            dispatch(fetchContactsSuccess(contacts));
        } catch (error) {
            dispatch(fetchContactsFailure(error));
        }
    };
};

export const fetchContact = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchContactRequest());
            const response = await axiosAPI.get("contacts/" + id + ".json");
            dispatch(fetchContactSuccess(response.data));
        } catch (error) {
            dispatch(fetchContactFailure(error));
        }
    };
};

export const putContact = (id, contact) => {
    return async (dispatch) => {
        try {
            dispatch(putContactsRequest());
            await axiosAPI.put("contacts/" + id + ".json", contact);
            dispatch(putContactsSuccess());
            dispatch(fetchContacts());
        } catch (error) {
            dispatch(putContactsFailure(error));
        }
    };
};

export const deleteContact = (id) => {
    return async (dispatch) => {
        try {
            dispatch(deleteContactsRequest());
            await axiosAPI.delete("contacts/" + id + ".json");
            dispatch(deleteContactsSuccess());
            dispatch(fetchContacts());
        } catch (error) {
            dispatch(deleteContactsFailure(error));
        }
    };
};
