import {
    DELETE_CONTACTS_FAILURE,
    DELETE_CONTACTS_REQUEST,
    DELETE_CONTACTS_SUCCESS,
    FETCH_CONTACTS_FAILURE,
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS,
    FETCH_CONTACT_FAILURE,
    FETCH_CONTACT_REQUEST,
    FETCH_CONTACT_SUCCESS,
    POST_CONTACTS_FAILURE,
    POST_CONTACTS_REQUEST,
    POST_CONTACTS_SUCCESS,
    PUT_CONTACTS_FAILURE,
    PUT_CONTACTS_REQUEST,
    PUT_CONTACTS_SUCCESS,
} from "../actions/contactsAction";

const initialState = {
    error: false,
    contacts: [],
    contact: [],
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_CONTACTS_REQUEST:
            return {...state};
        case POST_CONTACTS_SUCCESS:
            return {...state};
        case POST_CONTACTS_FAILURE:
            return {...state, error: action.error};
        case FETCH_CONTACTS_REQUEST:
            return {...state};
        case FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.contacts};
        case FETCH_CONTACTS_FAILURE:
            return {...state, error: action.error};
        case FETCH_CONTACT_REQUEST:
            return {...state};
        case FETCH_CONTACT_SUCCESS:
            return {...state, contact: action.contact};
        case FETCH_CONTACT_FAILURE:
            return {...state, error: action.error};
        case PUT_CONTACTS_REQUEST:
            return {...state};
        case PUT_CONTACTS_SUCCESS:
            return {...state};
        case PUT_CONTACTS_FAILURE:
            return {...state, error: action.error};
        case DELETE_CONTACTS_REQUEST:
            return {...state};
        case DELETE_CONTACTS_SUCCESS:
            return {...state};
        case DELETE_CONTACTS_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default contactsReducer;
