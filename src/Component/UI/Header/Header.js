import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className='head'>
            <h1>Contacts</h1>
            <ul className='items'>
                <li>
                    <button className='btn'><NavLink to={'/add'}>Add new Contact</NavLink></button>
                </li>
            </ul>
        </header>
    );
};

export default Header;