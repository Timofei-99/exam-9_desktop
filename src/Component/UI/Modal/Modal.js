import React from "react";
import "./Modal.css";
import Backdrop from "../Backdrop/Backdrop";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deleteContact} from "../../../store/actions/contactsAction";

const Modal = (props) => {
    const dispatch = useDispatch();

    const removeContact = (id) => {
        dispatch(deleteContact(id));
    };
    return (
        <>
            <Backdrop show={props.show} onClick={props.closed}/>
            <div
                className="Modal"
                style={{
                    transform: props.show ? "translateY(0)" : "translateY(-100vh)",
                    opacity: props.show ? "1" : "0",
                }}
            >
                <div>
                    <img src={props.photo} alt={props.name}/>
                </div>
                <div>
                    <h3>{props.name}</h3>
                    <p>{props.phone}</p>
                    <p>{props.email}</p>
                    <NavLink className="ModalBtn" to={"/edit-contacts/" + props.id}>
                        EDIT
                    </NavLink>
                    <button className="ModalBtn" onClick={() => removeContact(props.id)}>
                        DELETE
                    </button>
                </div>
                <button onClick={props.closed}>X</button>
            </div>
        </>
    );
};

export default Modal;
